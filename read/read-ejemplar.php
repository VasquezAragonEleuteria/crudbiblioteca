<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información de libro</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $clave_ejemplar = $_GET['clave_ejemplar'];
  $conservacion_ejemplar = $_GET['conservacion_ejemplar'];
  $isbn = $_GET['isbn'];
  $error = false;
  if (empty($clave_ejemplar)) {
    $error = true;
?>
  <p>Error, no se ha indicado clave</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar, conservacion_ejemplar,isbn
      from biblioteca.ejemplar
      where isbn = '".$isbn."' and clave_ejemplar='".$clave_ejemplar."';";

    $ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
      $error = true;
?>
  <p>No se ha encontrado algún ejemplar con clave <?php echo $clave_ejemplar; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($ejemplar, null, PGSQL_ASSOC);
      $conservacion_ejemplar = $tupla['conservacion_ejemplar'];
      $isbn = $tupla['isbn'];
?>
<table>
  <caption>Información de ejemplar</caption>
  <tbody>
    <tr>
      <th>clave_ejemplar</th>
      <td><?php echo $clave_ejemplar; ?></td>
    </tr>
    <tr>
      <th>conservacion</th>
      <td><?php echo $conservacion_ejemplar; ?></td>
    </tr>
    <tr>
      <th>isbn</th>
      <td><?php echo $isbn; ?></td>
    </tr>
     <th>Libro/s</th>
      <td>
<?php
      $query = "select titulo_libro
        from biblioteca.libro LA
        inner join biblioteca.ejemplar as A
          on (LA.isbn = A.isbn )where A.isbn='".$isbn."' and A.clave_ejemplar='".$clave_ejemplar."';";

      $libros = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($libros) == 0) {
?>
        <p>No tiene libros</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($libros, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
    
      <tr>
      <th>Autor/es</th>
      <td>
<?php
      $query = "select nombre_autor
        from biblioteca.libro_autor as LA
        inner join biblioteca.autor as A
          on (LA.id_autor = A.id_autor and LA.isbn = '".$isbn."');";

      $autores = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($autores) == 0) {
?>
        <p>Sin autor</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($autores, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
<?php
      }
    }
  
?>
    </tr>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);

?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de ejemplares</a></li>
</ul>

</body>
</html>
