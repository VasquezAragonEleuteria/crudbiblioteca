<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de Autores</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $id_autor = $_GET['id_autor'];

  if (empty($id_autor)) {
?>
  <p>Error, no se ha indicado el id del autor</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select id_autor, nombre_autor
      from biblioteca.autor
      where id_autor = '".$id_autor."';";

    $autor = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($autor) == 0) {
?>
  <p>No se ha encontrado algún autor con Id <?php echo $id_autor; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($autor, null, PGSQL_ASSOC);
      $nombre_autor = $tupla['nombre_autor'];
?>
<form action="update-autor.php" method="post">
<table>
  <caption>Información de Autor </caption>
  <tbody>
    <tr>
      <th>Id</th>
      <td><input type="text" name="id_autor" value="<?php echo $id_autor; ?>" /></td>
    </tr>
    <tr>
      <th>nombre</th>
      <td><textarea name="nombre_autor"><?php echo $nombre_autor; ?></textarea></td>
    </tr>
  </tbody>
</table>
<input type="submit" name="submit" value="UPDATE" />
</form>
<?php
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="autores.php">Lista de Autores</a></li>
</ul>

</body>
</html>
