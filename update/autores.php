<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Listado de Autores</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<?php
  $servidorbd = "localhost";
  $nombrebd = "prueba";
  $usuariobd= "programador";
  $contraseniabd = "12345";

  $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
  or die('No se ha podido conectar: ' . pg_last_error());

  $query = 'select id_autor, nombre_autor from biblioteca.autor';

  $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());
?>

<body>
<table>
  <caption>Listado de Autores</caption>
  <thead>
    <tr>
      <th>#</th>
      <th>Id</th>
      <th>Nombre</th>
      <th>Opción</th>
    </tr>
  </thead>
  <tbody>

<?php
  $contador = 1;
  while ($tupla = pg_fetch_array($resultado, null, PGSQL_ASSOC)) {
    $id_autor = $tupla['id_autor'];
?>
    <tr>
      <td>
        <?php echo $contador++; ?>
      </td>
<?php
    foreach ($tupla as $atributo) {
?>
      <td><?php echo trim($atributo); ?></td>
<?php
    }
?>
      <td>
        <a href="formulario-autor.php?id_autor=<?php echo $id_autor; ?>">Editar Información</a>
      </td>
    </tr>
<?php
  }

  pg_free_result($result);
  pg_close($dbconn);
?>

  </tbody>
</table>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
</ul>

</body>
</html>
