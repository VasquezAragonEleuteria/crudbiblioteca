<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de ejemplares</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<form action="create-ejemplar.php" method="post">
<table>
  <caption>Información de Ejemplar</caption>
  <tbody>
    <tr>
      <th>clave de ejemplar</th>
      <td><input type="text" name="clave_ejemplar" /></td>
    </tr>
    <tr>
      <th>Conservacion</th>
      <td><textarea name="conservacion_ejemplar"></textarea></td>
    </tr>
    <tr>
      <th>ISBN</th>
      <td><textarea name="isbn"></textarea></td>
    </tr>
  </tbody>
</table>
<input type="submit" name="submit" value="CREATE" />
</form>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
</ul>

</body>
</html>